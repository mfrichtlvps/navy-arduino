/*
Matt Frichtl
Anthony Meier
Vision Point Systems

Written for:
NRL code 6138
Corrosions Division

Version 2.0: Updated during assembly of the second wear testing table.

This code is used for running the abrasion wear tester. It performs the tests specified in MIL-PRF-24667C, Paragraphs 4.5.2 and 4.5.4. It cycles
through a number of menus to prompt the user to as to what test they want to run. If the user knows what test they would like to run, the program will
walk the user through and make sure that the correct connections are made and that the correct panel is installed. It will then stop at the correct
pre-specified times and remind the user as to what to do.

If the user does not want to run a pre-specified test, a custom set up can be used. Then based on the sample size the motor will run at a predetermined
speed for a duration given by the user in a number of cycles.

There can also be maintenance done to the set up too, where the motor can only run at a slow speed purely to reposition the drag table with out it being
forced by hand.

  Circuit Description

  Combines a standard 20x4 LCD display with Hitachi HD44780 driver with a
  3x4 matrix keypad.

  The circuit:
 * LCD RS pin to digital pin 14 (analog pin 0)
 * LCD Enable pin to digital pin 15 (analog pin 1)
 * LCD D4 pin to digital pin 16 (analog pin 2)
 * LCD D5 pin to digital pin 17 (analog pin 3)
 * LCD D6 pin to digital pin 18 (analog pin 4)
 * LCD D7 pin to digital pin 19 (analog pin 5)
 * LCD R/W pin to ground
 * Keypad pin 1 to digital pin 2
 * Keypad pin 2 to digital pin 3
 * Keypad pin 3 to digital pin 4
 * Keypad pin 4 to digital pin 5
 * Keypad pin 5 to digital pin 6
 * Keypad pin 6 to digital pin 7
 * Keypad pin 7 to digital pin 8
 * Digital pin 11 for software cancel button
 * Digital pin 9 to the motor controller
 * Digital pin 10 to the cycle counter switch
*/

#include <LiquidCrystal.h>
#include <Keypad.h>
#include <math.h>

/*  Debug and calibration modes.

    Debug mode displays some additional information during use to let the user
    know where in the program the unit is.

    Calibration mode allows the user to to correlate the desired cycles per
    minute with the motor speed values that are used in the program.

    Change both of these to false to run the device in normal mode.
*/

const boolean kDebugMode = false;
const boolean kCalibrationMode = false;

/*  Motor signal voltage for the two drive arms.  0-255 correlates to a 0-5V
    signal.  The maximum speed is set by the max speed trim pot in the motor
    controller box.  The difference between the long and short drive arm signals
    is to obtain roughly the same number of cycles per minute between each of
    the drive arms.  Adjust these values only if there is a discrepancy between
    the two drive arms.  If faster or slower operation of the table is desired,
    the user should refer to the signal isolator board documentation and Adjust
    the max trim pot accordingly.
*/

const int kShortDriveArmMotorSpeed = 175;
const int kLongDriveArmMotorSpeed = 255;

/*
    Cycle sensor sampling rate in milliseconds. Adjust this if the unit is
    counting each cycle multiple times due to the contact time with the magnetic
    switch.
*/

const int kCycleSensorSamplingDelay = 500;  // Sampling delay in ms

/*  Password to unlock the device.  */

const int kUnlockPassword = 6130;

/*
    Initialize the keypad. Only change the rowPins and colPins if you wish to
    change the Arduino pins used for connecting the keypad.
*/

const int keypadRows = 4;
const int keypadColumns = 3;
char keyLayout[keypadRows][keypadColumns] = {
  {'1','2','3'},
  {'4','5','6'},
  {'7','8','9'},
  {'*','0','#'}
  };
byte rowPins[keypadRows] = {5, 4, 3, 2}; //connect to the row pinouts of the keypad
byte colPins[keypadColumns] = {8, 7, 6}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad(makeKeymap(keyLayout), rowPins, colPins, keypadRows, keypadColumns);

LiquidCrystal lcd(A0, A1, A2, A3, A4, A5);
const byte kLcdRows = 4;
const byte kLcdColumns = 20;

//byte cursorRow = 3;
//byte cursorCol = 0;


//  Arduino pin initializations. Each pair has a HIGH and LOW pin.

const byte kMotorControllerPin = 9;
const byte kCycleCounterSwitchPin = 10;
const byte kCancelPin = 11;


//  Function Prototypes -- Don't change unless you know what you're doing

void setup();
void loop();
void menu();
char mainMenu();
void printToScreen(String print_string, byte row, byte column, boolean clear_screen);
char selectNonskidType();
boolean userSecurePanel();
boolean panelBreakInPrompt();
int getNumberCycles(char main_menu_choice);
byte selectNumberPhases(char main_menu_choice);
boolean userConfirmNumber();
void userInchMotor();
void lockScreen();
byte charToInt(char character);
void invalidResponse();
boolean buildTestProcedure(char main_menu_choice);
boolean runPhase(int number_cycles, int motor_speed, byte phase_number, byte number_phases);
boolean panelBreakIn(int motor_speed, byte number_phases);
boolean massCheck();
boolean userCancelTest();
void operationError();
void runMotor(int motor_speed);
void stopMotor();
boolean testSuccessful();
void printToScreen(String print_string, byte row, byte column, boolean clear_screen);
boolean userLoadWeight(byte panel_size);
boolean userReplaceWire();
byte userInputNumberPhases();
int userInputNumberCycles();
int userInputNumber();
void phaseCompletePause();
void unitCalibrationMode();


/*  Program starts here.  */

void setup() {
	lcd.begin(kLcdColumns, kLcdRows);
	lcd.clear();

  /*  The pin modes can be changed if needed.  */
	pinMode(kCycleCounterSwitchPin, OUTPUT);
  digitalWrite(kCycleCounterSwitchPin, LOW);
	pinMode(kCancelPin, OUTPUT);
  digitalWrite(kCancelPin, LOW);
  pinMode(kMotorControllerPin, OUTPUT);
}

void loop() {
	lockScreen();
}

void menu() {
	/*
    This is the main program that is run to select a test. It prompts the user to input all parameters then calls the appropriate subroutines.
    The user inputs are declared below:

    nonskid_type -	The type of coating being tested. Affects the number of cycles as explained below. The options are between Composition G and L non-skid coatings.
    number_phases - The number of 150 cycle phases for the tests. All non-skid types except Type V go through 3, 150 cycle phases (total of 500 cycles with panel break-in.
                The Type V coatings undergo an 2 additional 150 cycle testing phases (total of 800 cycles including panel break-in).
    number_cycles -	The number of cycles will get input *if* the user opts to run a custom test. Otherwise, the number of cycles will be determined by the test that gets selected.
                It is assumed that due to inertia of the table, 2 additional cycles will be completed following motor stop.
    repeatTest	-	A boolean value for repeating the test. If the user chooses to repeat the test, it evaluates to true after the test completes and false otherwise.
    break_in_phase -	A boolean value to indicate that a test requires a 50 cycle break-in phase, default is true.

    All of these variables are defined in MIL-PRF-24467C and are fixed based upon the desired test being run. If the user decides to run a custom test,
    they can specify all variables.
	*/

    while (true) {
        if (kDebugMode == true) {
          printToScreen(F("ENTERING MAIN MENU"), (byte)0, (byte)0, true);
          delay(1000);
        }

        char nonskid_type, test_panel_size, main_menu_choice;
        byte number_phases;
        int number_cycles, motor_speed;

        boolean testing_complete = false;
        boolean repeatTest = true;
        boolean composition_G_test = false;	//	Used for replacing the abrasion wire in composition G tests

        main_menu_choice = mainMenu();

        switch(main_menu_choice) {
            case'1': // Composition G
            if (kDebugMode == true) {
              printToScreen(F("COMPOSITION G"), (byte)0, (byte)0, true);
              printToScreen(F("TEST SELECTED"), (byte)1, (byte)0, false);
              delay(1000);
            }
            testing_complete = buildTestProcedure(main_menu_choice);
                break;
            case'2': // Composition L
            if (kDebugMode == true) {
              printToScreen(F("COMPOSITION L"), (byte)0, (byte)0, true);
              printToScreen(F("TEST SELECTED"), (byte)1, (byte)0, false);
              delay(1000);
            }
            testing_complete = buildTestProcedure(main_menu_choice);
            break;
            case'3':
            if (kDebugMode == true) {
              printToScreen(F("CUSTOM"), (byte)0, (byte)0, true);
              printToScreen(F("TEST SELECTED"), (byte)1, (byte)0, false);
              delay(1000);
            }
            testing_complete = buildTestProcedure(main_menu_choice);
                break;
            case'4':
            lockScreen();
                break;
            case'#':
                menu();
            break;
            default:
                invalidResponse();
        }

        if (testing_complete) {
          testSuccessful();
        } else {
          printToScreen(F("ERROR DURING TESTING"), (byte)0, (byte)0, true);
          delay(2000);
          lockScreen();
        }
    }
}


char mainMenu() {
	/*
    Displays the main menu where the user can pick a Composition G or L test,
    run a custom test, enter maintenance mode, or lock the device.

    Returns a char with the following options:
        '1':	MIL-PRF-24667 Composition G test
        '2':	MIL-PRF-24667 Composition L test
        '3':	Custom tests. Subroutines guide user through selection of
          parameters for testing.
        '4':	Maintenance mode. Subroutines jog motor at low rate of speed to
          slowly move table.
        '5':	Lock unit.
	*/
	while(true) {
		printToScreen(F("MAIN MENU (1/2): #->"), (byte)0, (byte)0, true);
		printToScreen(F("SELECT DESIRED TEST"), (byte)1, (byte)0, false);
		printToScreen(F("1. MIL-24667C COMP G"), (byte)2, (byte)0, false);
		printToScreen(F("2. MIL-24667C COMP L"), (byte)3, (byte)0, false);
		char user_selection = keypad.waitForKey();

		if(user_selection == '#') {
			printToScreen(F("MAIN MENU (2/2): #<-"), (byte)0, (byte)0, true);
			printToScreen(F("3. CUSTOM TEST "), (byte)1, (byte)0, false);
//			printToScreen(F("4. INCH MOTOR"), (byte)2, (byte)0, false);
			printToScreen(F("4. LOCK"), (byte)2, (byte)0, false);
			user_selection = keypad.waitForKey();
		}

		if(user_selection == '#') {
      mainMenu();
    } else if ((user_selection != '1') && (user_selection != '2') && (user_selection != '3') && (user_selection != '4') && (user_selection != '5')) {
      invalidResponse();
    } else {
      return user_selection;
    }
	}
}


boolean buildTestProcedure(char main_menu_choice) {
	/*
    This is the function that runs the specified test. It is not used for
    maintenance mode. It evaluates to true if successful or false otherwise.
    All parameters come from MIL-PRF-24667C for wear testing of Composition G
    and L non-skid panels. An additional phase is added if the user is
    programming a custom test with a break-in phase.

    byte panel_size = 6, 12, or 18 for 6"x12", 12"x12", and 18"x18" panels.
  */

    int number_phases = selectNumberPhases(main_menu_choice);
    int number_cycles = getNumberCycles(main_menu_choice);
    byte panel_size = selectTestPanelSize(main_menu_choice);
    int motor_speed = selectMotorSpeed(panel_size);
    boolean break_in_phase = selectBreakInPhase(main_menu_choice);
    boolean testing_complete = false;
    boolean composition_L_test = false;
    if (main_menu_choice == '2') {
    composition_L_test = true;
    } else if (main_menu_choice == '3' && break_in_phase) {
    number_phases++;
    }
    if (userSecurePanel()) {
    if (userLoadWeight(panel_size)) {
      testing_complete = runTestProcedure(number_phases, number_cycles, motor_speed, break_in_phase, composition_L_test);
    }
    }
    return testing_complete;
}


void userInchMotor() {
	/*	This function runs the maintenance mode. It's intended to inch the motor
      while the cancel button is depressed.
	*/

	printToScreen(F("INCH THE MOTOR"), (byte)0, (byte)0, true);
	printToScreen(F("WITH CANCEL BUTTON"), (byte)1, (byte)0, false);
	printToScreen(F("0. Exit"), (byte)3, (byte)0, false);

  boolean motor_running = false;
	while(true) {
		char key = keypad.getKey();
		while(kCancelPin == HIGH) {
      if (!motor_running) {
			  runMotor(kShortDriveArmMotorSpeed);
        motor_running = true;
      }
		}
		stopMotor();
    motor_running = false;
		if(key == '0') {
			return;
		}
	}
}


byte selectNumberPhases(char main_menu_choice) {
  /*  Check the non-skid type that is being tested and return appropriate number
      of phases, including the break-in phase or phases. Values for
      number_phases are from MIL-PRF-24667. Paragraph 4.5.4 is for Composition G
      and Paragraph 4.5.2 is for Composition L.  Composition G requires the user
      to select a non-skid type that affects the total number of phases while
      Composition L is fixed at 6 phases, which includes a break-in phase of 50
      cycles and a test phase of 200 cycles for 3 separate panels.
  */

char nonskid_type;

  switch(main_menu_choice) {
    case '1': // Composition G
      nonskid_type = selectNonskidType();
      if (nonskid_type == '1') { // Types I-IV and VI-X
        return 4;
      } else { // Type V
        return 6;
      }
    case '2':	// Composition L
      return 6;
    case '3': // Custom test
      return userInputNumberPhases();
    default:
      operationError();
      break;
  }
}


boolean userSecurePanel() {
	/*	Subroutine that prompts the user to secure the test panel to the wear
      table. Returns true if user indicates panel is ready and false otherwise.
	*/

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("SECURE TEST PANEL"), (byte)0, (byte)0, true);
    	printToScreen(F("1. CONTINUE"), (byte)2, (byte)0, false);
    	printToScreen(F("2. CANCEL"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();

    switch(user_selection) {
      case '1':
        return true;
      case '2':
        userCancelTest();
        refresh_screen = true;
        break;
      default:
        invalidResponse();
        refresh_screen = true;
        break;
    }
  }
  operationError();
}


int getNumberCycles(char main_menu_choice) {
	/*	Prompts the user to input the number of cycles in each phase for a custom
      test. Allows for 1-999 cycles. Returns an int with the number of cycles
      input.

	*/

  if (main_menu_choice == '1') {
    return 150;
  } else if (main_menu_choice == '2') {
    return 200;
  } else if (main_menu_choice == '3') {
    return userInputNumberCycles();
  } else {
    operationError();
  }
}


int userInputNumberCycles() {
  /*  This function allows the user to input the number of cycles desired.
      It returns an integer value corresponding to the number input on the LCD
      screen using the keypad.
  */

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("INPUT CYCLES PER"), (byte)0, (byte)0, true);
      printToScreen(F("TEST PHASE"), (byte)1, (byte)0, false);
      refresh_screen = false;
    }
    int number_cycles = userInputNumber();

    if (number_cycles == -1) {
      ;
    } else if (number_cycles == 0)	{
      invalidResponse();
      refresh_screen = true;
    } else {
      printToScreen(String(number_cycles), (byte)0, (byte)0, true);
      lcd.print(" CYCLES PER PHASE");
      printToScreen(F("PLEASE CONFIRM"), (byte)1, (byte)0, false);
      boolean user_confirm_cycles = userConfirmNumber();
      if (user_confirm_cycles) {
        return number_cycles;
      } else {
        refresh_screen = true;
      }
    }
  }
}


byte userInputNumberPhases() {
	/*	Prompts the user to select the number of phases desired during testing
      between 1-256. Returns a byte corresponding to the number of phases input.
      If -1 is returned by userInputNumber(), it indicates that the user hit
      the cancel button, but decided to return to input the number of phases.
	*/

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("INPUT PHASES"), (byte)0, (byte)0, true);
      printToScreen(F("FOR TESTING"), (byte)1, (byte)0, false);
      refresh_screen = false;
    }
    int number_phases = userInputNumber();
    if (number_phases == -1) {
      ;
    } else if (number_phases == 0)	{ 	//	Don't allow input of 0 phases.
      invalidResponse();
      refresh_screen = true;
    } else {
      printToScreen(String(number_phases), (byte)0, (byte)0, true);
      lcd.print(" PHASES");
      printToScreen(F("PLEASE CONFIRM"), (byte)1, (byte)0, false);
      boolean user_confirm_phases = userConfirmNumber();
      if (user_confirm_phases) {
        return number_phases;
      } else {
        refresh_screen = true;
      }
    }
  }
}


int userInputNumber() {
  /*  This function is a slave function that allows the user to input a number
      and returns the number as an integer to the parent function. It does not
      check if that number makes sense. It returns -1 if the user pressed
      cancel, but decided to return to input the number.
  */

    int number = 0;
		byte vals[10] = {0,0,0,0,0,0,0,0,0,0};
		int exp = -1;
    byte digit_count = 0;
    boolean looping = true;
    boolean refresh_screen = true;

    while (looping) {
      if (refresh_screen) {
        printToScreen(F("# CONFIRM"), (byte)2, (byte)10, false);
        printToScreen(F("* CANCEL"), (byte)3, (byte)10, false);
        refresh_screen = false;
      }
    	int key = charToInt(keypad.waitForKey());
      switch(key) {
				default:
					lcd.setCursor(digit_count,3);
					lcd.print(key);
					vals[digit_count] = key;
					exp++;
          digit_count++;
					break;
				case 10:
					looping = false;
					break;
				case 11:
					userCancelTest();
          refresh_screen = true;
          return -1;
			}
		}
		for(byte x=0;x<=9;x++) {
			number += vals[x]*pow(10, exp);
			exp--;
		}
    if (digit_count > 2) {
      number++;
    }
	return number;
}


boolean userConfirmNumber() {
	/*	This function displays the confirm number dialog to allow the user to
      confirm input values for phases, cycles, speeds, etc.
		  Returns true of confirmed and false otherwise.
	*/

	printToScreen(F("1: YES"), (byte)2, (byte)0, false);
	printToScreen(F("2: NO"), (byte)3, (byte)0, false);

	if(keypad.waitForKey() == '1') {
		return true;
	}	else {
		return false;
	}
}


boolean runTestProcedure(int number_phases, int number_cycles, int motor_speed, boolean break_in_phase, boolean composition_L_test) {
  /*  This function runs the test procedure that was built by buildTestProcedure.
      It returns true if it thinks the testing completed sucessfully and false
      otherwise.
  */

	int phase_counter = 1;
	while(phase_counter <= number_phases) {
		boolean mass_check_complete = false;
		if(break_in_phase) {
			boolean break_in_complete = panelBreakIn(motor_speed, phase_counter, number_phases);
			if(break_in_complete) {
        ++phase_counter;
        break_in_phase = false;
      } else {
        printToScreen(F("BREAK-IN FAILED"), (byte)0, (byte)0, true);
        delay(2000);
        operationError();
				break;
			}
		}
    boolean phase_complete = runPhase(number_cycles, motor_speed, phase_counter, number_phases);
    if(phase_complete) {
      if(phase_counter == number_phases) {
        return true;
      } else {
        ++phase_counter;
        boolean cable_replaced = false;
        if (!composition_L_test) {
          while(!cable_replaced) {
            cable_replaced = userReplaceWire();
          }
        } else {
          break_in_phase = true;
          userCompositionLPrompt();
          boolean continue_test = false;
          while (!continue_test) {
            continue_test = userSecurePanel();
          }
        }
      }
    } else {
      printToScreen(F("PHASE INCOMPLETE"), (byte)0, (byte)0, true);
      delay(2000);
      operationError();
    }
	}
  printToScreen(F("TESTING FAILED"), (byte)0, (byte)0, true);
  delay(2000);
  operationError();
  return false;
}


boolean runPhase(int number_cycles, int motor_speed, byte phase_number, byte number_phases) {
	/*
		This is the main function of the program that will execute the test phases.
    For custom tests the number of cycles and motor speed will be	based on user
    input. This function will return true upon successful completion and false
    if it thinks there's an error.

    The cycle counter switch is programmed with a slight delay to avoid
    multiple counts per cycle. The delay is adjustable via kCycleCounterSwitch,
    if needed.

    The unit stops with the cycle counter magnetic switch in contact with the
    magnet, so the delay before entering the loop prevents 5 cycles from being
    counted at the start of the test.
	*/

	boolean test_running = true;
  boolean refresh_screen = true;
	int cycle_counter = 0;
	runMotor(motor_speed);
  delay(1000);  // Delay is necessary to prevent counter from starting too soon.

	while((cycle_counter / 2 < number_cycles) && test_running) {
		if (digitalRead(kCycleCounterSwitchPin) == HIGH) {
      cycle_counter++;
      printToScreen(F("CYCLE "), (byte)2, (byte)0, false);
      lcd.print(cycle_counter / 2);
      lcd.print(F(" OF "));
      lcd.print(number_cycles);
			delay(kCycleSensorSamplingDelay);
		} else if (digitalRead(kCancelPin) == HIGH) {
  			stopMotor();
  			test_running = userCancelTest();
        runMotor(motor_speed);
        refresh_screen = true;
		} else if (refresh_screen) {
        printToScreen(F("PHASE "), (byte)0, (byte)0, true);
        lcd.print(phase_number);
        lcd.print(F(" OF "));
        lcd.print(number_phases);
        printToScreen(F("CYCLE "), (byte)2, (byte)0, false);
        lcd.print(cycle_counter / 2 );
        lcd.print(F(" OF "));
        lcd.print(number_cycles);
        refresh_screen = false;
      }
	}
	stopMotor();
	if(cycle_counter / 2 == number_cycles) {
		return true;
	} else {
    printToScreen(F("RUN PHASE FAILED"), (byte)0, (byte)0, true);
    delay(2000);
    operationError();
		return false;
	}
}


void userCompositionLPrompt() {
  /*  This function prompts the user to replace the steel rod with an abrasion
      cable. It cancels testing if the user so chooses.
  */

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("PHASE COMPLETE"), (byte)0, (byte)0, true);
      printToScreen(F("INSTALL CABLE"), (byte)1, (byte)0, false);
      printToScreen(F("1. CONTINUE"), (byte)2, (byte)0, false);
      printToScreen(F("2. CANCEL"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();
    switch(user_selection) {
      case '1':
        return;
      case '2':
        userCancelTest();
        refresh_screen = true;
        break;
      default:
        invalidResponse();
        refresh_screen = true;
        break;
    }
  }
}


void phaseCompletePause() {
  /*  This function lets the user know that a phase is complete and pauses the
      testing to allow the user to make any needed changes before continuing
      the test.
  */

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("PHASE COMPLETE"), (byte)0, (byte)0, true);
      printToScreen(F("1. CONTINUE"), (byte)2, (byte)0, false);
      printToScreen(F("2. CANCEL"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();
    switch(user_selection) {
      case '1':
        return;
      case '2':
        userCancelTest();
        refresh_screen = true;
        break;
      default:
        invalidResponse();
        refresh_screen = true;
        break;
    }
  }
}


boolean panelBreakIn(int motor_speed, byte phase_counter, byte number_phases) {
  /*  This function performs the panel break-in. It returns true if the break-in
      was completed successfully and false otherwise.
  */

	printToScreen(F("STARTING BREAK-IN:"), (byte)0, (byte)0, true);
	delay(1000);

	boolean break_in_complete = runPhase(50, motor_speed, phase_counter, number_phases);
	boolean wire_replaced = userReplaceWire();
	return(break_in_complete && wire_replaced);
}


boolean testSuccessful() {
	/*	Displays a test success message then forces the user to acknowledge and
      exit to the lock screen.
  */

  boolean refresh_screen = true;

	while(true) {
    if (refresh_screen) {
      printToScreen(F("TESTING COMPLETE"), (byte)0, (byte)0, true);
    	printToScreen(F("1. LOCK UNIT"), (byte)2, (byte)0, false);
    	printToScreen(F("2. MAIN MENU"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
		char user_selection = keypad.waitForKey();

		switch(user_selection) {
			case '1':
				lockScreen();
        break;
			case '2':
				menu();
        break;
			default:
				invalidResponse();
        refresh_screen = true;
				break;
		}
	}
  printToScreen(F("ERROR AT TEST"), (byte)0, (byte)0, true);
  printToScreen(F("COMPLETE SCREEN"), (byte)0, (byte)0, false);
  delay(2000);
	operationError();
}


void runMotor(int motor_speed) {
	/*	This is a slave function that really really loves running the motor and nothing more.
	*/

	analogWrite(kMotorControllerPin, motor_speed);
	return;
}


void stopMotor() {
	//	This is a slave function who's only purpose in life is to stop the motor.
	analogWrite(kMotorControllerPin, 0);
	return;
}


void operationError() {
	/*  Whoops. Something happened that wasn't expected. This will prompt the user
      that something unexpected happened and force him to restart. Sorry.
  */

	char user_selection = '1';

	printToScreen(F("SOMETHING UNEXPECTED"), (byte)0, (byte)0,true);
	printToScreen(F("HAS HAPPENED"), (byte)1, (byte)0,false);
	printToScreen(F("LOCKING UNIT"), (byte)2, (byte)0,false);
  delay(2000);
  lockScreen();
}


void printToScreen(String print_string, byte row, byte column, boolean clear_screen) {
	/*	This function is used to print on the lcd screen. It requires a string to
      know what to print, an int to know which row to print it on, another int for
      the column and a boolean to know if it needs to clear the screen prior to printing.
	*/

	if(clear_screen) {
		lcd.clear();
	}
	lcd.setCursor(column,row);
	lcd.print(print_string);
	return;
}


void lockScreen() {
  /*  This function displays the lock screen and prompts the user to input the
      password.

      If the unit is in debug mode, it is bypassed.
      If the unit is in calibration mode, this function calls the special
      calibration mode function.
  */

  if (kDebugMode) {
    printToScreen(F("DEBUG MODE"), (byte)0, (byte)0, true);
    printToScreen(F("BYPASSING LOCK"), (byte)1, (byte)0, false);
    delay(1000);
    menu();
  } else if (kCalibrationMode){
    printToScreen(F("CALIBRATION MODE"), (byte)0, (byte)0, true);
    printToScreen(F("BYPASSING LOCK"), (byte)1, (byte)0, false);
    delay(1000);
    unitCalibrationMode();
  }

	int codeEntry = 0;	//used for temporary storage of password entry
	//displays prompt for password entry
	printToScreen(F("UNIT LOCKED"), (byte)0, (byte)0, true);
	printToScreen(F("INPUT PASSWORD:"), (byte)1, (byte)0, false);
	codeEntry = (codeEntry + (1000 * charToInt(keypad.waitForKey())));
	printToScreen(F("*"), (byte)3, (byte)0, false);
	codeEntry = (codeEntry + (100 * charToInt(keypad.waitForKey())));
	printToScreen(F("*"), (byte)3, (byte)1, false);
	codeEntry = (codeEntry + (10 * charToInt(keypad.waitForKey())));
	printToScreen(F("*"), (byte)3, (byte)2, false);
	codeEntry = (codeEntry + charToInt(keypad.waitForKey()));
	printToScreen(F("*"), (byte)3, (byte)3, false);

	if(codeEntry == kUnlockPassword) {
		//display a successful entry message
		printToScreen(F("UNIT UNLOCKED"), (byte)0, (byte)0, true);
		delay(1000);
		menu();
	} else {
		//display an unsuccessful entry message and then repeats lockScreen()
		printToScreen(F("INCORRECT PASSWORD"), (byte)0, (byte)0, true);
		delay(1000);
		lockScreen();
	}
}


byte charToInt(char character) {
	/*	This function converts the chars from the keypad to actual integers for
    numeric operation. For '#' it returns 10 and for '*' it returns 11, so this
    must be handled by the calling function. Returns an integer.
	*/
	switch(character) {
		case '#':
			return 10;
		case '*':
			return 11;
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;
	}
}


void invalidResponse() {
	/*  This function is run whenever a user's response is invalid. It returns to
      the selection screen that the user came from.
  */

	printToScreen(F("INVALID RESPONSE"), (byte)0, (byte)0, true);
	printToScreen(F("PLEASE SELECT A"), (byte)1, (byte)0, false);
	printToScreen(F("VALID CHOICE"), (byte)2, (byte)0, false);
  delay(1000);
	return;
}


boolean userCancelTest() {
	/*	This function pauses and/or cancels the test that is currently running in
      two steps. It stops the motor and prompts the user to choose between
      resuming the test and cancelling the test.

      The function returns "true" if the user chooses to resume testing.
  */

  boolean refresh_screen = true;

	while(true) {
    if (refresh_screen) {
      printToScreen(F("CONFIRM CANCEL"), (byte)0, (byte)0, true);
    	printToScreen(F("1: RETURN"), (byte)2, (byte)0, false);
    	printToScreen(F("2: CANCEL & LOCK"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
		char user_selection = keypad.waitForKey();
		switch(user_selection) {
			case '1':
				return true;
			case '2':
				printToScreen(F("TEST CANCELLED"), (byte)0, (byte)0, true);
				printToScreen(F("LOCKING UNIT"), (byte)1, (byte)0, false);
				delay(2000);
				lockScreen();
        break;
			default:
				invalidResponse();
        refresh_screen = true;
        break;
		}
	}
	return false;	//	Raises error if it gets here.
}


char selectNonskidType() {
  /*	Prompts the user to select the coating type.
    Returns a char with the following values:
      '1':	Type I-IV or VI-X
      '2':	Type V
  */

  boolean refresh_screen = true;

  while(true) {
    if (refresh_screen) {
      printToScreen(F("NONSKID TYPE:"), (byte)0, (byte)0, true);
      printToScreen(F("1: TYPES I-IV/VI-X"), (byte)1, (byte)0, false);
      printToScreen(F("2: TYPE V"), (byte)2, (byte)0, false);
      printToScreen(F("3: MAIN MENU"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();

    switch(user_selection) {
      case '1':
        return user_selection;
      case '2':
        return user_selection;
      case '3':
        menu();
        break;
      default:
        invalidResponse();
        refresh_screen = true;
        break;
    }
  }
  printToScreen(F("ERROR SELECTING"), (byte)0, (byte)0, true);
  printToScreen(F("NONSKID TYPE"), (byte)1, (byte)0, true);
  delay(2000);
  operationError();
}


int selectMotorSpeed(byte panel_size) {
  /*  This function checks the main menu choice and returns the appropriate
      motor speed from MIL-PRF-24667C or prompts the user to select a motor
      speed, if a custom test is desired. The speed is the same for both
      6"x12" and 12"x12" panels.
  */

  while (true) {
    switch (panel_size) {
      case 6: //6" by 12" panel size
        return kShortDriveArmMotorSpeed;
      case 12: //12" by 12" panel size
        return kShortDriveArmMotorSpeed;
      case 18: //18" by 18" panel size
        return kLongDriveArmMotorSpeed;
      default:
        operationError();
        break;
    }
  }
  operationError();
}


boolean selectBreakInPhase(char main_menu_choice) {
  /*  This function returns a boolean indicating whether a break-in phase is
      required for the desired test. It should always return true unless the
      user is requesting a custom test without a break-in phase.
  */

  switch(main_menu_choice) {
    case '1': //Composition G
      return true;
    case '2': //Composition L
      return true;
    case '3': //Custom Test
      return panelBreakInPrompt();
    default:
      operationError();
      break;
  }
}


boolean panelBreakInPrompt() {
  /*  This function prompts the user to select whether a panel break-in is needed
      when a custom test is being performed. It returns a boolean indicating the
      user's response.
  */

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("PERFORM BREAK-IN?"), (byte)0, (byte)0, true);
      printToScreen(F("1. YES"), (byte)2, (byte)0, false);
      printToScreen(F("2. N0"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();

    switch(user_selection) {
      case '1':
        return true;
      case '2':
        return false;
      default:
        invalidResponse();
        refresh_screen = true;
      break;
    }
  }
  operationError();
}


int selectTestPanelSize(char main_menu_choice) {
  /*  This function returns the appropriate test panel size based on the test.
      If the user desires a custom test, then he is prompted to input the
      selected panel size.
  */

  switch(main_menu_choice) {
    case '1':
      return 6;
    case '2':
      return 6;
    default:
      break;
  }

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("SELECT PANEL SIZE"), (byte)0, (byte)0, true);
      printToScreen(F("1. 6 x 12"), (byte)1, (byte)0, false);
      printToScreen(F("2. 12 x 12"), (byte)2, (byte)0, false);
      printToScreen(F("3. 18 x 18"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();

    switch(user_selection) {
      case '1':
        return 6;
      case '2':
        return 12;
      case '3':
        return 18;
      default:
        invalidResponse();
        refresh_screen = true;
        break;
    }
  }
  printToScreen(F("ERROR SELECTING"), (byte)0, (byte)0, true);
  printToScreen(F("PANEL SIZE"), (byte)1, (byte)0, false);
  delay(2000);
  operationError();
}


boolean userLoadWeight(byte panel_size) {
  /*  This function prompts the user to load the test apparatus with the
      appropriate amount of weight based on the panel size.
  */

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      switch(panel_size) {
        case 6:
          printToScreen(F("LOAD 30 POUNDS"), (byte)0, (byte)0, true);
          break;
        case 12:
          printToScreen(F("LOAD 60 POUNDS"), (byte)0, (byte)0, true);
          break;
        case 18:
          printToScreen(F("LOAD 90 POUNDS"), (byte)0, (byte)0, true);
          break;
        default:
          printToScreen(F("UNKNOWN PANEL SIZE"), (byte)0, (byte)0, true);
          delay(2000);
          operationError();
          break;
      }
      printToScreen(F("1. CONTINUE"), (byte)2, (byte)0, false);
      printToScreen(F("2. CANCEL"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();

    switch(user_selection) {
      case '1':
        return true;
      case '2':
        userCancelTest();
        refresh_screen = true;
        break;
      default:
        invalidResponse();
        refresh_screen = true;
        break;
    }
  }
  printToScreen(F("LOAD WEIGHT ERROR"), (byte)0, (byte)0, true);
  delay(2000);
  operationError();
}


boolean userReplaceWire() {
  /*	This function stops the test and prompts the user to replace the wire
      during testing.  It returns true if the user tells the machine that the
      wire is replaced and false otherwise.
  */

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("PHASE COMPLETED"), (byte)0, (byte)0, true);
      printToScreen(F("REPLACE WIRE"), (byte)1, (byte)0, false);
      printToScreen(F("1. CONTINUE"), (byte)2,(byte)0, false);
      printToScreen(F("2. CANCEL"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();

    switch(user_selection) {
      case '1':
        return true;
      case '2':
        userCancelTest();
        refresh_screen = true;
        break;
      default:
        invalidResponse();
        refresh_screen = true;
        break;
    }
  }
  printToScreen(F("ERROR AT WIRE"), (byte)0, (byte)0, true);
  printToScreen(F("REPLACEMENT CHECK"), (byte)1, (byte)0, false);
  delay(2000);
  operationError();
}


void unitCalibrationMode() {
  /*  This function is used to calibrate the program to the motor. It measures
      the cycles per minute and displays it on the LCD screen. It prompts the
      user to select which drive arm is attached to the motor and then runs the
      motor at the speed defined by the corresponding speed variable at the
      head of this program. The user must use this information to adjust the
      speed variable appropriately to achieve the desired cycles per minute.
  */

  printToScreen(F("CALIBRATION MODE"), (byte)0, (byte)0, true);
  delay(2000);
  int motor_speed = selectDriveArmSize();
  runCalibrationMode(motor_speed);
}


void runCalibrationMode(int motor_speed) {
  /*  This function runs the motor and operates the LCD during calibration.
  */

  boolean test_running = true;
  boolean refresh_screen = true;
  float time_start = (float) millis();
  float cycle_counter = 0;
  float timer = 0;
  float cycles_per_minute = 0.0;
  runMotor(motor_speed);

  while (test_running) {
		if (digitalRead(kCycleCounterSwitchPin) == HIGH) {
			cycle_counter++;
      printToScreen(String((int)cycle_counter), (byte)0, (byte)8, false);
			delay(kCycleSensorSamplingDelay);
		} else if (digitalRead(kCancelPin) == HIGH) {
			stopMotor();
			test_running = userCancelTest();
      refresh_screen = true;
      runMotor(motor_speed);
		} else if (refresh_screen) {
      printToScreen(F("CYCLES: 0"), (byte)0, (byte)0, true);
      printToScreen(F("TIME: "), (byte)1, (byte)0, false);
      printToScreen(F("CYCLES/MIN: "), (byte)2, (byte)0, false);
      printToScreen(F("SPEED VALUE: "), (byte)3, (byte)0, false);
      printToScreen(String(motor_speed), (byte)3, (byte)13, false);
      refresh_screen = false;
    }
    timer = (millis() - time_start) / 1000;
    cycles_per_minute = (cycle_counter / timer) * 60.0;
    printToScreen(String((int)timer), (byte)1, (byte)6, false);
    printToScreen(F("    "), (byte)2, (byte)12, false);
    printToScreen(String(cycles_per_minute), (byte)2, (byte)12, false);
	}
}


int selectDriveArmSize() {
  /*  This function prompts the user to select the drive arm size that is
      attached to the unit for calibration.
  */

  boolean refresh_screen = true;

  while (true) {
    if (refresh_screen) {
      printToScreen(F("SELECT DRIVE ARM"), (byte)0, (byte)0, true);
      printToScreen(F("1. SHORT"), (byte)2,(byte)0, false);
      printToScreen(F("2. LONG"), (byte)3, (byte)0, false);
      refresh_screen = false;
    }
    char user_selection = keypad.waitForKey();

    switch(user_selection) {
      case '1':
        return kShortDriveArmMotorSpeed;
      case '2':
        return kLongDriveArmMotorSpeed;
      default:
        invalidResponse();
        refresh_screen = true;
        break;
    }
  }
}
