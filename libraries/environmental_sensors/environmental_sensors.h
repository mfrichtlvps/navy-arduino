/* A unified sensor library for many different kinds of environmental sensors.

Licence info here
written by Matthew Frichtl, Vision Point Systems
*/
#ifndef environmental_sensors_h
#define environmental_sensors_h

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif

#include "Adafruit_MAX31865.h"

// Generic sensor class

class Sensor {
  public:
    virtual void Attach(String) { };
    virtual void Begin(String) { };
    virtual uint16_t GetState(void) { };
    virtual String Report(void) {return("Sensor"); };
    virtual String Initialize(void) {return("Initialize"); };
};

// Pressure transducer class

class PT : public Sensor {
  public:
    PT(uint8_t pin, bool criticalGlobal, uint16_t min_pressure, uint16_t max_pressure, uint16_t fail_pressure);
    uint16_t GetState(void);
    void Attach(String);
    void Begin(String);
    void SetTransducerPressures(uint16_t min_pressure, uint16_t max_pressure, uint16_t fail_pressure);
    String Report(void);
    String Initialize(void);

  private:
    uint16_t _fail_pressure, _min_pressure, _max_pressure,  _reading, _sensor_id;
    uint8_t _pin;
    bool _criticalGlobal;
    float _voltage, _pressure;
    String _message, _name, _panel, _type, _unit;
};


class DigitalRTD : public Sensor {
  public:
    DigitalRTD(Adafruit_MAX31865& rtd, bool criticalGlobal);
    uint16_t GetState(void);
    void Attach(String);
    void Begin(String);
    String Report(void);
    String Initialize(void);

  private:
    int8_t _CS, _DI, _DO, _CLK, _wires;
    int16_t _RRef;
    uint8_t _failure_point, _fault;
    bool _criticalGlobal;
    long _sensor_id;
    Adafruit_MAX31865& _rtd;
    String _message, _name, _panel, _type, _unit;
};


class DigitalSwitch : public Sensor {
  public:
    DigitalSwitch(uint8_t pin, bool criticalGlobal);
    uint16_t GetState(void);
    void Attach(String);
    void Begin(String);
    String Report(void);
    String Initialize(void);

  private:
    uint8_t _pin;
    bool _criticalGlobal;
    long _sensor_id;
    String  _failure_point, _message, _name, _panel, _type, _unit;
};
#endif
