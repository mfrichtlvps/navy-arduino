/* A unified sensor library for many different kinds of environmental sensors.

"criticalGlobal" sensors can send all attached chambers into a fault state, stopping
any ongoing tests.

Licence info here
written by Matthew Frichtl, Vision Point Systems
*/

#include "environmental_sensors.h"
#include "Adafruit_MAX31865.h"

// Pressure transducers

PT::PT(uint8_t pin, bool criticalGlobal, uint16_t min_pressure, uint16_t max_pressure, uint16_t fail_pressure) {
  _pin = pin;
  _criticalGlobal = criticalGlobal;
  _min_pressure = min_pressure;
  _max_pressure = max_pressure;
  _fail_pressure = fail_pressure;
}

void PT::Attach(String kivyPanelName) {  // Attach the sensor to a Kivy panel
  _panel = kivyPanelName;
}

void PT::Begin(String name) {
  pinMode(_pin, INPUT);
  _sensor_id = random(2147483647);
  _name = name;
  _type = "pressure";
  _unit = "psi";
}

uint16_t PT::GetState(void) {
  _reading = analogRead(_pin);
  _voltage = _reading * (5.0 / 1023.0);
  return(_min_pressure + _max_pressure * (_voltage / 5.0));
}

String PT::Initialize(void) {
  _message = "I_";
  _message += _panel;
  _message += "_";
  _message += _type;
  _message += "_";
  _message += _sensor_id;
  _message += "_";
  _message += _name;
  _message += "_";
  _message += GetState();
  _message += "_";
  _message += _fail_pressure;
  _message += "_";
  _message += _unit;
  _message += "_";
  _message += _criticalGlobal;
  _message += "_";
  return(_message);
}

String PT::Report(void) {
  _message = "U_";
  _message += _sensor_id;
  _message += "_";
  _message += GetState();
  _message += "_";
  return(_message);
}

void PT::SetTransducerPressures(uint16_t min_pressure, uint16_t max_pressure, uint16_t fail_pressure) {
  _min_pressure = min_pressure;
  _max_pressure = max_pressure;
  _fail_pressure = fail_pressure;
}

// Digital rtd
DigitalRTD::DigitalRTD(Adafruit_MAX31865& rtd, bool criticalGlobal) : _rtd(rtd) {
  _criticalGlobal = criticalGlobal;
  _RRef = 430;
}

void DigitalRTD::Attach(String kivyPanelName) {  // Attach the sensor to a Kivy panel
  _panel = kivyPanelName;
}

void DigitalRTD::Begin(String name) {
  _failure_point = 0;
  _sensor_id = random(2147483647);
  _name = name;
  _type = "RTD";
  _unit = "F";
  _rtd.begin(MAX31865_4WIRE);
}

uint16_t DigitalRTD::GetState(void) {
  _fault = _rtd.readFault();
  if (_fault) {
    _rtd.clearFault();
    return(0);
  } else {
    return(_rtd.temperature(100, _RRef));
  }
}

String DigitalRTD::Initialize(void) {
  _message = "I_";
  _message += _panel;
  _message += "_";
  _message += _type;
  _message += "_";
  _message += _sensor_id;
  _message += "_";
  _message += _name;
  _message += "_";
  _message += GetState();
  _message += "_";
  _message += _failure_point;
  _message += "_";
  _message += _unit;
  _message += "_";
  _message += _criticalGlobal;
  _message += "_";
  return(_message);
}

String DigitalRTD::Report(void) {
  _message = "U_";
  _message += _sensor_id;
  _message += "_";
  _message += GetState();
  _message += "_";
  return(_message);
}

// Digital switches can sense high and low states of physical switches or relays
DigitalSwitch::DigitalSwitch(uint8_t pin, bool criticalGlobal) {
  _pin = pin;
  _criticalGlobal = criticalGlobal;
}

void DigitalSwitch::Attach(String kivyPanelName) {  // Attach the sensor to a Kivy panel
  _panel = kivyPanelName;
}

void DigitalSwitch::Begin(String name) {
  pinMode(_pin, INPUT_PULLUP);
  _sensor_id = random(2147483647);
  _name = name;
  _failure_point= "NA";
  _type = "powerSense";
  _unit = "boolean";
}

uint16_t DigitalSwitch::GetState(void) {
  return(digitalRead(_pin));
}

String DigitalSwitch::Initialize(void) {
  _message = "I_";
  _message += _panel;
  _message += "_";
  _message += _type;
  _message += "_";
  _message += _sensor_id;
  _message += "_";
  _message += _name;
  _message += "_";
  _message += GetState();
  _message += "_";
  _message += _failure_point;
  _message += "_";
  _message += _unit;
  _message += "_";
  _message += _criticalGlobal;
  _message += "_";
  return(_message);
}

String DigitalSwitch::Report(void) {
  _message = "U_";
  _message += _sensor_id;
  _message += "_";
  _message += GetState();
  _message += "_";
  return(_message);
}
