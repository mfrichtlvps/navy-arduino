/*
  Airflow sensor script.
*/

#include <SPI.h>
#include <SD.h>
#include <Wire.h>
#include <RTClib.h>

Sd2Card card;
SdVolume volume;
SdFile root;
const int chipSelect = 4;
const long sampleDelay = 900000;
const int inputPinChannelOne = A0;
const int inputPinChannelTwo = A1;
const int inputPinChannelThree = A2;
const int numberReadings = 25;
float measurement[numberReadings];
String channelName;
unsigned long now;
unsigned long lastReading = 0;
String filename = "LOGGER00.CSV";
RTC_DS3231 rtc;

void setup() {
    Serial.begin(9600);
    Serial.println("hello");
    for (int readIndex = 0; readIndex < numberReadings; readIndex++) {
      measurement[readIndex] = 0;
    }
    millis();
    pinMode(inputPinChannelOne, INPUT);
    pinMode(inputPinChannelTwo, INPUT);
    pinMode(inputPinChannelThree, INPUT);

    if (!SD.begin(chipSelect)) {
        Serial.println("Card init. failed!");
    }

    filename = openFile(filename);
    if (! rtc.begin()) {
        Serial.println("Couldn't find RTC");
    while (1);
    }

    if (rtc.lostPower()) {
        Serial.println("RTC lost power, lets set the time!");
        // following line sets the RTC to the date & time this sketch was compiled
        rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
        // This line sets the RTC with an explicit date & time, for example to set
        // January 21, 2014 at 3am you would call:
        // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
    }
}

void loop() {
    now = millis();
    if (now - lastReading > sampleDelay) {
        File dataFile = SD.open(filename, FILE_WRITE);
        dataFile.println(measureAirflow(inputPinChannelOne));
        dataFile.println(measureAirflow(inputPinChannelTwo));
        dataFile.println(measureAirflow(inputPinChannelThree));
        dataFile.close();
        lastReading = millis();
    }
}

String measureAirflow(const int channel) {
    float total = 0;
    for (int readIndex = 0; readIndex < numberReadings; readIndex++) {
        measurement[readIndex] = analogRead(channel) * (3.3 / 1024);
        total = total + measurement[readIndex];
        delay(50);
    }
    if (channel == A0) {
        channelName = "1";
    } else if (channel == A1) {
        channelName = "2";
    } else {
        channelName = "3";
    }

    float airflow = (200 * ((total / numberReadings) / 3.3 - 0.5)) / 0.4;
    DateTime now = rtc.now();

    String dataString = (String) now.year();
    dataString += "/";
    dataString += (String) now.month();
    dataString += "/";
    dataString += (String) now.day();
    dataString += " ";
    dataString += (String) now.hour();
    dataString += ":";
    dataString += (String) now.minute();
    dataString += ":";
    dataString += (String) now.second();
    dataString += ",";
    dataString += channelName;
    dataString += ",";
    dataString += airflow;
    Serial.println(dataString);
    return(dataString);
}


String openFile(String filename) {
    for (uint8_t i = 0; i < 100; i++) {
        filename[6] = i/10 + '0';
        filename[7] = i%10 + '0';
        if (! SD.exists(filename)) {
            break;
        }
    }
    return(filename);
}
