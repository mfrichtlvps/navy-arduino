/*
 Reads input voltages and writes log files to the SD card.
 
 Reads up to 6 analog input pins and maps the result to a range from 0 to 5000 mV.
 The number of readings per second is set by the frequency value.
 The program also creates up to 100 new data files on the SD card.
 This version of the file outputs to the serial monitor as well.
 
 */

#include <SD.h>     // SD card library, needed for data logging.

int debug = 1;  //Change to enable/disable debug mode (Serial monitor)

// Change this to match your SD shield or module as follows:
// Arduino Ethernet shield: pin 4
// Adafruit SD shields and modules: pin 10
// Sparkfun SD shield: pin 8
const int chipSelect = 10;

//  Change the number of channels used - Approximate max frequencies:
//  1 channel:   58Hz
//  2 channels:  56-57Hz
//  3 channels:  53-55Hz
//  4 channels:  50-53Hz
//  5 channels:  51Hz
//  6 channels:  49Hz
//    The frequencies appear to fluctuate slightly from test to test.

const int channels = 4;

// Don't change anything below unless you know what you're doing.

int sensorValue;
int outputValue;
char filename[] = "LOGGER00.CSV";

void setup() {

  pinMode(10,OUTPUT);
  millis();
  if (debug==1)  {
    Serial.begin(9600);
  }
  
  // Check to make sure the card is present and can be initialized:
  if (!SD.begin(chipSelect) & debug==1) {
    Serial.println("Card failed or not present...");
    return; 
  }
  
  // Create a new file.
  for (uint8_t i = 0; i < 100; i++) {
    filename[6] = i/10 + '0';
    filename[7] = i%10 + '0';
    if (! SD.exists(filename)) {       // Only opens the file if it doesn't exist
      break;
    }
  }
}

void loop() {
  
	File dataFile = SD.open(filename, FILE_WRITE);
        if (!SD.exists(filename) & debug==1)  {
          Serial.println("File does not exist!");
        }
	String dataLine = "";
  	dataLine += millis();
	int count=0;
	while (count<channels)	{
		dataLine += ",";
		sensorValue = analogRead(count);
		outputValue = sensorValue * (5.0 / 1023.0) * 1000;
		dataLine += outputValue;
		count++;
    }
  if (debug==1)  {
    Serial.println(dataLine);
  }
  if (dataFile)  {
    dataFile.println(dataLine);
    dataFile.close();  
  }
  else  {
    Serial.print("Error opening ");
    Serial.println(filename);
  }
}
