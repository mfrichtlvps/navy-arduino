/* Environmental monitor PLC code.
*/

// Depends on the following Arduino libraries:
// - Adafruit Unified Sensor Library: https://github.com/adafruit/Adafruit_Sensor

#include <environmental_sensors.h>
#include <Adafruit_MAX31865.h>

// Define I/O pins for controls
const int WATER_PRESSURE_PIN = A1;
const int AIR_PRESSURE_PIN = A0;
const int CHAMBER_1_POWER_PIN = 4;
const int CHAMBER_1_BT_PT_PIN = A4;
const int CHAMBER_2_POWER_PIN = 3;
const int CHAMBER_2_BT_PT_PIN = A3;
const int CHAMBER_2_BT_RTD_PIN = 10;
const int CHAMBER_2_BT_RTD_WIRES = 4;
const int CHAMBER_3_POWER_PIN = 2;
const int CHAMBER_3_BT_PT_PIN = A2;

// Equipment definitions for Kivy tabs
Adafruit_MAX31865 CHAMBER_2_BT_RTD = Adafruit_MAX31865(10, 11, 12, 13);

const int sensor_number = 8;
Sensor *sensor_array[] = {
  new PT(WATER_PRESSURE_PIN, true, 0, 150, 50),
  new PT(AIR_PRESSURE_PIN, true, 0, 150, 15),
  new DigitalSwitch(CHAMBER_1_POWER_PIN, false),
  new PT(CHAMBER_1_BT_PT_PIN, false, 0, 30, 3),
  new DigitalSwitch(CHAMBER_2_POWER_PIN, false),
  new PT(CHAMBER_2_BT_PT_PIN, false, 0, 30, 3),
  //new DigitalRTD(CHAMBER_2_BT_RTD, false),
  new DigitalSwitch(CHAMBER_3_POWER_PIN, false),
  new PT(CHAMBER_3_BT_PT_PIN, false, 0, 30, 3)
};

String panels[sensor_number] = {
  "SERVICES",
  "SERVICES",
  "CHAMBER 1",
  "CHAMBER 1",
  "CHAMBER 2",
  "CHAMBER 2",
  //"CHAMBER 2",
  "CHAMBER 3",
  "CHAMBER 3"
};

String names[sensor_number] = {
  "AIR PRESSURE",
  "DI PRESSURE",
  "CHAMBER POWER",
  "BT PRESSURE",
  "CHAMBER POWER",
  "BT PRESSURE",
  //"BT TEMPERATURE",
  "CHAMBER POWER",
  "BT PRESSURE"
};

bool initialized = false;
bool newCommand = false;
bool resultCheck = false;
char inputChar;
char* reports[sensor_number];

void setup() {
  Serial.begin(115200);
  beginSensors();
}

void loop() {
  if (newCommand == true) {
    processCommands();
  }
}

void serialEvent() {
  inputChar = Serial.read();
  newCommand = true;
}

void sendUpdates(void) {
  boolean sent_report = false;
  for (int i = 0; i < sensor_number; i++) {
    Serial.println(sensor_array[i]->Report());
  }
}

void initializeSensors(void) {
  for (int i = 0; i < sensor_number; i++) {
    Serial.println(sensor_array[i]->Initialize());
  }
}

void processCommands() {
  switch (inputChar) {
    case 'U':
      sendUpdates();
      break;
    case 'I':
      initializeSensors();
      break;
    default:
      break;
  }
  newCommand = false;
}

void beginSensors(void) {
  for (int i = 0; i < sensor_number; i++) {
    sensor_array[i]->Begin(names[i]);
    sensor_array[i]->Attach(panels[i]);
  }
}
