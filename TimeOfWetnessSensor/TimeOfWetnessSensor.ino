/*
    This script is for the time-of-wetness sensor developed for use at Vision
    Point Systems.  It can handle millis() rolling over after 49 days.
 */

#include <SD.h>     // SD card library, needed for data logging.

/*
    Change this to match your SD shield or module as follows:
    Arduino Ethernet shield: pin 4
    Adafruit SD shields and modules: pin 10
    Sparkfun SD shield: pin 8
*/
const int chipSelect = 10;

int inputPin = 2;   // inputPin is read to identify any completed circuits.
char filename[] = "LOGGER00.CSV";   // initialize the first file name
float interval = 900000;   // sampling interval in milliseconds
unsigned long previousMillis = 0;   // initialize previousMillis for the timer
int days = 0;
int hours = 0;
int minutes = 0;
int seconds = 0;
int counter = 1;
String reading = "none";

void setup() {
    pinMode(inputPin, INPUT_PULLUP);

    // Check to make sure the card is present and can be initialized:
    if (!SD.begin(chipSelect)) {
        return;
    }

    Serial.begin(9600);

    // Create a new file.
    for (uint8_t i = 0; i < 100; i++) {
        filename[6] = i/10 + '0';
        filename[7] = i%10 + '0';
        if (! SD.exists(filename)) {  // Only opens the file if it doesn't exist
            break;
        }
    }
    File dataFile = SD.open(filename, FILE_WRITE);
    dataFile.println("Reading, Elapsed Time (DD:HH:MM:SS), Reading");
    dataFile.close();
    Serial.println("Running time-of-wetness sensor.");
}

void loop() {
    unsigned long currentMillis = millis();
    unsigned long increment = currentMillis - previousMillis;
    if (increment >= interval) {
        int wetnessSensor = digitalRead(inputPin);
        if (wetnessSensor == 1) {
            reading = "dry";
        } else {
            reading = "wet";
        }
        seconds += increment / 1000;
        while (seconds >= 60) {
            minutes++;
            if (minutes >= 60) {
                hours++;
                minutes = minutes - 60;
            } else if (hours >= 24) {
                days++;
                hours = hours - 24;
            }
            seconds = seconds - 60;
        }
        
        File dataFile = SD.open(filename, FILE_WRITE);
        String dataLine = "";
        dataLine = dataLine + counter + ",";
        dataLine = dataLine + days + ":" + hours + ":" + minutes + ":" + seconds + ",";
        dataLine = dataLine + reading;
        dataFile.println(dataLine);
        dataFile.close();
        Serial.println(dataLine);
        previousMillis = currentMillis;
        counter++;
    }
}
